import React from "react";
import PropTypes from 'prop-types';

const Footer = (props) => {
  const { titleByDefault =  "Footer title" } = props;

  return (
    <>
      <h1>{props.title ? props.title : titleByDefault}</h1>
    </>
  );
};

Footer.propTypes = {
  title: PropTypes.string,
  titleByDefault: PropTypes.string
}

export default Footer;
