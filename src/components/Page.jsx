import React from "react";
import PropTypes from 'prop-types';

const Page = (props) => {
  return (
    <div className="page-container">
      <h1>{props.heading}</h1>
      <>
      {props.children}
      </>
    </div>
  );
};

Page.propTypes = {
  heading: PropTypes.string,
  children: PropTypes.object
}

export default Page;
