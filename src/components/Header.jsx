import React from "react";
import PropTypes from 'prop-types';


const Header = (props) => {
  const { titleByDefault } = props;

  return (
    <>
      <h1>{props.title ? props.title : titleByDefault}</h1>
    </>
  );
};

Header.defaultProps = {
  titleByDefault: "Default Header title"
}

Header.propTypes = {
  title: PropTypes.string,
  titleByDefault: PropTypes.string
}


export default Header;
