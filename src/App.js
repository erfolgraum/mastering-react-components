import './App.css';
import Page from './components/Page';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Page heading="Page component with props.children!!!">
          <Header title={"Greeting from Header component!"}/>
          <Main />
          <Footer title={"Greeting from Footer component!"}/>
      </Page>
    </div>
  );
}

export default App;
